﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private Animator animator;//0 -500 10
    private Transform parent;
    private MainController main;
    private SuperShot shot;
    private bool needOneMoreMove = false;
    [SerializeField] GameObject bulletPref, SupbulletPref;
    [SerializeField] Text score, deathscore;
    private int gameCoin = 0;
    public bool ShotReady = false, isImmortal = false;
    
    private void Start()
    {
        main = FindObjectOfType<MainController>();
        animator = GetComponent<Animator>();
        shot = FindObjectOfType<SuperShot>();
        animator.SetInteger("dir", 0);
        parent = transform.parent.transform;
        score.text = gameCoin+"";
    }

    public void ClickLeft()
    {
        if (!main.isFreezed && transform.position.x > -200)
        {
            if (transform.parent.position.x > 200 && animator.GetInteger("dir") != 0)
                needOneMoreMove = true;
            animator.SetInteger("dir", 2);
        }
    }

    public void ClickRight()
    {
        if (!main.isFreezed && transform.position.x < 200)
        {
            if (transform.parent.position.x < -200 && animator.GetInteger("dir") != 0)
                needOneMoreMove = true;
            animator.SetInteger("dir", 1);
        }
    }
    public void ClickShoot()
    {
        if (!main.isFreezed)
        {
            Instantiate(bulletPref, new Vector3(transform.position.x + Random.Range(-15f, 15f), -500, 10), Quaternion.AngleAxis(0, Vector3.up));
            StartCoroutine(SpawnSecondBullet());
        }
    }

    private IEnumerator SpawnSecondBullet()
    {
        yield return new WaitForSeconds(0.07f);
        Instantiate(bulletPref, new Vector3(transform.position.x - Random.Range(-15f, 15f), -500, 10), Quaternion.AngleAxis(0, Vector3.up));
    }

    public void AnimFinished()
    {
        if (needOneMoreMove)
            needOneMoreMove = false;
        else
            animator.SetInteger("dir", 0);
        parent.transform.position = transform.position;
        transform.localPosition = Vector3.zero;
        if (isImmortal)
        {
            shot.BatteryCharging();
            isImmortal = false;
            ShotReady = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isImmortal)
        {
            Debug.Log("Failed");
            main.GameOver();
            deathscore.text = "Вы набрали " + gameCoin;
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") + gameCoin);
            PlayerPrefs.Save();
        }

    }

    public void AddCoins (int x)
    {
        gameCoin = gameCoin + x;
        score.text = gameCoin + "";
    }

    public void SuperShot()
    {
        if (ShotReady)
        {
            Debug.Log("Анимация сработала");
            isImmortal = true;
            transform.tag = "Immortal";
            animator.SetInteger("dir", 3);
            shot.BatteryDraining();
        }
    }
}
