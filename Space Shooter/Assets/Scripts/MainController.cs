﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainController : MonoBehaviour {

    [SerializeField] GameObject bigAsteroidPref, littleAsteroidPref, panelDeath;
    [SerializeField] Sprite[] bigAsteroids, littleAsteroids;
    [SerializeField] Button but;

    Animator animator;

    public bool isFreezed;
    public float movingSpeed;
    private float distance;
    private enum SpawnVariants { NNN, NNB, NBN, BNN, NBB, BNB, BBN };//720 720 360
    List<SpawnVariants>[] variants;

    [SerializeField] Animator button;


    private void Start()
    {
        isFreezed = false;
        distance = -1000f;
        movingSpeed = 500f;
        variants = new List<SpawnVariants>[7];
        variants[(int)SpawnVariants.NNN] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NNB, SpawnVariants.NBN, SpawnVariants.BNN, SpawnVariants.NBB, SpawnVariants.BNB, SpawnVariants.BBN };
        variants[(int)SpawnVariants.NNB] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NNB, SpawnVariants.NBN, SpawnVariants.BNN, SpawnVariants.NBB, SpawnVariants.BNB };
        variants[(int)SpawnVariants.NBN] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NNB, SpawnVariants.NBN, SpawnVariants.BNN, SpawnVariants.BBN, SpawnVariants.NBB };
        variants[(int)SpawnVariants.BNN] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NNB, SpawnVariants.NBN, SpawnVariants.BNN, SpawnVariants.BBN, SpawnVariants.BNB };
        variants[(int)SpawnVariants.BBN] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NBN, SpawnVariants.BNN, SpawnVariants.BBN };
        variants[(int)SpawnVariants.NBB] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.NBN, SpawnVariants.NNB, SpawnVariants.NBB };
        variants[(int)SpawnVariants.BNB] = new List<SpawnVariants>() { SpawnVariants.NNN, SpawnVariants.BNN, SpawnVariants.NNB, SpawnVariants.BNB };
        animator = FindObjectOfType<PlayerController>().GetComponent<Animator>();
    }

    int curDist = 0, newDistBetweenAst = 1000;
    SpawnVariants curState = SpawnVariants.NNN;
    private void Update()
    {
        distance += movingSpeed * Time.deltaTime;
        if (curDist + newDistBetweenAst < distance)
        {
            curDist += newDistBetweenAst;
            newDistBetweenAst = Random.Range(1, 7) * 500;
            if (newDistBetweenAst == 6 * 500) newDistBetweenAst = 500;
            curState = variants[(int)curState][Random.Range(0, variants[(int)curState].Count)];
            SpawnNewAsteroids(curState);
        }
    }

    float asteroidSpawnOffset = 1500f;
    private void SpawnNewAsteroids(SpawnVariants v)
    {
        if (v == SpawnVariants.BBN || v == SpawnVariants.BNB || v == SpawnVariants.BNN)//Астероид слева
            Instantiate(bigAsteroidPref, new Vector3(-360, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward)).GetComponent<SpriteRenderer>().sprite = bigAsteroids[Random.Range(0, bigAsteroids.Length)];
        else if (Random.Range(0, 2) == 1)
            Instantiate(littleAsteroidPref, new Vector3(-360, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward));//not needed while we have 1 sprite .GetComponent<SpriteRenderer>().sprite = littleAsteroids[Random.Range(0, littleAsteroids.Length)];

        if (v == SpawnVariants.BBN || v == SpawnVariants.NBB || v == SpawnVariants.NBN)//Астероид в центре
            Instantiate(bigAsteroidPref, new Vector3(0, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward)).GetComponent<SpriteRenderer>().sprite = bigAsteroids[Random.Range(0, bigAsteroids.Length)];
        else if (Random.Range(0, 2) == 1)
            Instantiate(littleAsteroidPref, new Vector3(0, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward));//not needed while we have 1 sprite .GetComponent<SpriteRenderer>().sprite = littleAsteroids[Random.Range(0, littleAsteroids.Length)];

        if (v == SpawnVariants.BNB || v == SpawnVariants.NBB || v == SpawnVariants.NNB)//Астероид справа
            Instantiate(bigAsteroidPref, new Vector3(360, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward)).GetComponent<SpriteRenderer>().sprite = bigAsteroids[Random.Range(0, bigAsteroids.Length)];
        else if (Random.Range(0, 2) == 1)
            Instantiate(littleAsteroidPref, new Vector3(360, asteroidSpawnOffset + distance - curDist, 0), Quaternion.AngleAxis(Random.value * 360f, Vector3.forward));//not needed while we have 1 sprite .GetComponent<SpriteRenderer>().sprite = littleAsteroids[Random.Range(0, littleAsteroids.Length)];
    }

    public void GameOver()
    {
        FreezeAllObjects(true);
        panelDeath.SetActive(true);
        but.gameObject.SetActive(false);
    }

    public void FreezeAllObjects(bool freeze)
    {
        isFreezed = freeze;
        movingSpeed = 500*System.Convert.ToInt32(!freeze);
        foreach (FlyingBullet bullet in FindObjectsOfType<FlyingBullet>())
            bullet.speed = 3500f* System.Convert.ToInt32(!freeze);
        animator.speed = System.Convert.ToInt32(!freeze); 
    }
}
