﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipes : MonoBehaviour {

    PlayerController controller;
    private Vector2 StartPoint = Vector2.zero, EndPoint = Vector2.zero;

    // Use this for initialization
    void Start () {
        controller = FindObjectOfType<PlayerController>();
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount > 0)
        {            
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    {                        
                        StartPoint = touch.position;
                        //Debug.Log("Began " + touch.position.x+ " " + StartPoint.x);
                        break;
                    }
                case TouchPhase.Ended:
                    {                      
                        EndPoint = touch.position;
                        Vector2 swipe = new Vector2(EndPoint.x - StartPoint.x, EndPoint.y - StartPoint.y);
                        if (swipe.x > -swipe.y)
                        {
                            if (swipe.x > swipe.y)
                            {
                                Debug.Log("Одиночное вправо");
                                controller.ClickRight();
                            }
                            else
                            {
                                Debug.Log("СупеШОТ");
                                controller.SuperShot();
                            }
                        }
                        else if (swipe.x < swipe.y)
                        {
                            Debug.Log("Одиночное влево");
                            controller.ClickLeft();
                        }
                            break;
                    }                    
            }            
        }
	}
}
