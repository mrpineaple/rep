﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BigAsteroidController : MonoBehaviour {

    private MainController main;
    private PlayerController player;

    private void Start ()
    {
        main = FindObjectOfType<MainController>();
        player = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        transform.Translate(0, -main.movingSpeed * Time.deltaTime, 0, Space.World);
        if (transform.position.y < -1500)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Immortal"))
            StartCoroutine(Die());
    }

    private IEnumerator Die()
    {
        yield return new WaitForSeconds(0.07f);
        player.AddCoins(15);
        Destroy(gameObject);
    }
}
