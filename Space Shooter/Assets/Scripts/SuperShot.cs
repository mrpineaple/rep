﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperShot : MonoBehaviour {

    Animator anim;
    PlayerController controller;
    /*[SerializeField]*/ float ChargingDuration = 5f;

    public void Start()
    {
        //ChargingDuration = 1f;
        controller = FindObjectOfType<PlayerController>();
        anim = GetComponent<Animator>();
        BatteryCharging();
    }

    public void BatteryCharging()
    {
        anim.SetBool("BatteryLoading", true);
    }

    public void BatteryDraining()
    {
        anim.SetBool("BatteryLoading", false);
    }

    public void ShotReady()
    {
        controller.ShotReady = true;
    }   
}
