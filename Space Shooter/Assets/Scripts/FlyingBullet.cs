﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBullet : MonoBehaviour {

    public float speed = 3500f;

    private void Update()
    {
        transform.Translate(0, speed*Time.deltaTime, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Wall") || collision.transform.CompareTag("lAst") || collision.transform.CompareTag("bAst"))
            Destroy(gameObject);
    }
}
