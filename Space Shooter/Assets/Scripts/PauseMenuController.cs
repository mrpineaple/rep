﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {

    [SerializeField] GameObject panelPause;
    private bool isPaused = false;
    private MainController main;

    private void Start()
    {
        main = FindObjectOfType<MainController>();
    }


    public void ClickSettings()
    {
        main.FreezeAllObjects(!isPaused);
        panelPause.SetActive(!isPaused);
        isPaused = !isPaused;
    }

   

    public void ClickRestart()
    {
        SceneManager.LoadScene("Assets/Scenes/main.unity");
    }

    public void ClickMenu()
    {
        SceneManager.LoadScene("Assets/Scenes/menu.unity");
    }



    // Update is called once per frame
    void Update () {
		
	}
}
