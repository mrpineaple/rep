﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MenuController : MonoBehaviour {


    [SerializeField] Animator an; //К чему прикреплена анимация
    [SerializeField] GameObject SettingsButton, CreditsButton, LanguageButton;
    [SerializeField] Image SoundButtonImage;
    [SerializeField] Text score;

    [SerializeField] Sprite SoundOn, SoundOff;


    private bool state = false, sound = true;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("State"))
        {
            PlayerPrefs.SetInt("State", 1);
            PlayerPrefs.SetInt("Coins", 0);
            score.text = "Всего очков: 0";
        } else
        {
            score.text = "Всего очков: " + PlayerPrefs.GetInt("Coins");
        }

    }


        public void ClickStart()
    {
        SceneManager.LoadScene("Assets/Scenes/main.unity");

    }

    public void ClickSetting()
    {
        Debug.Log("Настройки");
        if (!state)
        {
            an.SetBool("State", true);
            state = true;
        } else
        {
            an.SetBool("State", false);
            state = false;
        }
    }

    public void ClickSound()
    {
        Debug.Log("Звук");
        //Настройки звука

        if (sound)
        {
            SoundButtonImage.sprite = SoundOff;
            sound = false;
            //Отключение звука
        }
        else
        {
            SoundButtonImage.sprite = SoundOn;
            sound = true;
            //Включение звука
        }
    }

    public void ClickLanguage()
    {
        Debug.Log("Язык");
        //Настройки языка
    }

    public void ClickCredit()
    {
        Debug.Log("Информация");
        //Информация
    }
}
