﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;//привет

public class LittleAsteroidController : MonoBehaviour {

    private int health = 2;
    private MainController main;
    private PlayerController player;

    private void Start()
    {
        main = FindObjectOfType<MainController>();
        player = FindObjectOfType<PlayerController>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (--health <= 0)
            StartCoroutine(Die());
        if (collision.transform.CompareTag("Immortal"))
        {
            StartCoroutine(Die());
        }
    }

    private IEnumerator Die()
    {
        yield return new WaitForSeconds(0.07f);
        player.AddCoins(5);
        Destroy(gameObject);
    }

    private void Update()
    {
        transform.Translate(0, -main.movingSpeed * Time.deltaTime, 0, Space.World);
        if (transform.position.y < -1500)
            Destroy(gameObject);
    }
}
